
**************
Brush Settings
**************

.. figure:: /images/sculpt-paint_weight-paint_tool-settings_brush-settings_brush-panel.png
   :align: right
   :width: 200

   Brush settings panel.

Information on brush settings for every mode can be found in these pages:

:doc:`Brush </sculpt_paint/brush/brush_settings>`
   General and advanced settings.

:doc:`Stroke </sculpt_paint/brush/stroke>`
   Stroke methods and settings.

:doc:`Falloff </sculpt_paint/brush/falloff>`
   Falloff curves and settings.

:doc:`Cursor </sculpt_paint/brush/cursor>`
   Cursor and appearance settings.

.. toctree::
   :hidden:

   Stroke </sculpt_paint/brush/stroke.rst>
   Falloff </sculpt_paint/brush/falloff.rst>
   Cursor </sculpt_paint/brush/cursor.rst>