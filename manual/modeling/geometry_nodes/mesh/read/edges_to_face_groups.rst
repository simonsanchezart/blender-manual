.. index:: Geometry Nodes; Edges to Face Groups
.. _bpy.types.GeometryNodeEdgesToFaceGroups:

*************************
Edges to Face Groups Node
*************************

.. figure:: /images/node-types_GeometryNodeEdgesToFaceGroups.png
   :align: right
   :alt: Edges to Face Groups Node.

The *Edges to Face Groups* node group faces into regions surrounded by the selected boundary edges.


Inputs
======

Boundary Edges
   Edges used to split faces into separate groups.


Properties
==========

This node has no properties.


Outputs
=======

Face Group ID
   Index of the face group inside each boundary edge region.
